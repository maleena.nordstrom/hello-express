# Hello-express

This project contains REST API with following features:

- Welcome endpoint
- Multiply endpoint

## Getting started

First install the dependencies

```sh
npm i
```

Start the REST API service:

`node app.js`

Or in the latests NodeJS version (>=20):

`node --watch app.js`

TO DO:
- [x] Task 1
- [ ] Task 2
- [ ] Task 3

![kitten](/kitten.jpg)

